json.array!(@students) do |student|
  json.extract! student, :id, :Studentid, :allowed, :user_id
  json.url student_url(student, format: :json)
end
